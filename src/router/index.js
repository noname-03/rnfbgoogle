import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from '../screen/home';
import Qr from '../screen/qr';
import CrashlythicsScreen from '../screen/crashlytics';
import Map from '../screen/map';
import LoginScreen from '../screen/login'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="CrashlythicsScreen" component={CrashlythicsScreen} />
      <Tab.Screen name="Qr" component={Qr} />
      <Tab.Screen name="Map" component={Map} />
    </Tab.Navigator>
  )
}

const Router = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }} />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{ headerShown: false }} />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="CrashlythicsScreen"
        component={CrashlythicsScreen}
        options={{ headerShown: false }}
      />
      {/* <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }} /> */}
    </Stack.Navigator>
  );
};

export default Router;
