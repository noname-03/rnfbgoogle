import { View, Text, Button, Alert, TouchableHighlight, } from 'react-native';
import React, { useEffect, useState } from 'react';
import messaging from '@react-native-firebase/messaging';
import TouchID from 'react-native-touch-id';

const Home = ({ navigation }) => {
  const onSetupCloudMessaging = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  };

  const getToken = async () => {
    const token = await messaging().getToken();
    // alert(JSON.stringify(token));
    console.log(JSON.stringify(token));
  };

  useEffect(() => {
    onSetupCloudMessaging();
    getToken();
  }, []);

  const optionalConfigObject = {
    title: 'Authentication Required', // Android
    imageColor: '#e00606', // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: 'Touch sensor', // Android
    sensorErrorDescription: 'Failed', // Android
  };

  const pressHandler = () => {
    TouchID.authenticate('to demo this react-native component', optionalConfigObject)
      .then(success => {
        navigation.navigate('CrashlythicsScreen');
      })
      .catch(error => {
        alert('Authentication Failed');
      });
  }

  // useEffect(() => {
  //   const unsubscribe = messaging().onMessage(async remoteMessage => {
  //     Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
  //   });

  //   return unsubscribe;
  // }, []);

  return (
    <View style={{ display: 'flex', margin: 4 }}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          margin: 4,
        }}>
        <Text style={{ color: 'black' }}>Challange Chapter 6</Text>
        <Text style={{ color: 'black' }}>Fahrurrozi - UCIC</Text>
      </View>
      <View
        style={{
          margin: 4,
        }}>
        <Button
          title="crashlythics"
          onPress={() => navigation.navigate('CrashlythicsScreen')}
        />
        <TouchableHighlight onPress={() => pressHandler()}>
          <Text>
            Authenticate with Touch ID
          </Text>
        </TouchableHighlight>
      </View>
    </View>
  );
};

export default Home;
