import { View, Text, Alert } from 'react-native'
import React from 'react'
import { CameraScreen, CameraType } from 'react-native-camera-kit'
import { useIsFocused } from '@react-navigation/native'


const Qr = props => {
    const IsFocused = useIsFocused();
    const onReadCode = (data) => {
        alert(data.nativeEvent.codeStringValue)
    }
    return (
        IsFocused ?
            <CameraScreen
                CameraType={CameraType.Back}
                scanBarcode={true}
                onReadCode={(event) => onReadCode(event)}
                showFrame={true}
                laserColor='red'
                frameColor='white'
            />
            :
            null
    )
}

export default Qr