import React, {useEffect} from 'react';
import {View, Button} from 'react-native';
import crashlytics from '@react-native-firebase/crashlytics';

async function onSignIn(user) {
  crashlytics().log('User signed in.');
  await Promise.all([
    crashlytics().setUserId(user.uid),
    crashlytics().setAttribute('credits', String(user.credits)),
    crashlytics().setAttributes({
      role: 'member',
      followers: '14',
      email: user.email,
      username: user.username,
    }),
  ]);
}
export default function CrashlythicsScreen() {
  useEffect(() => {
    crashlytics().log('app mounted.');
  }, []);

  return (
    <View>
      <Button
        title="Sign In"
        onPress={() =>
          onSignIn({
            uid: 'Aa0Bb1Cc2DdsaEe4FfGg6Hh7Ii8Jj',
            username: 'Fahrurrozi',
            email: 'fahrurrozi@mail.com',
            credits: 42,
          })
        }
      />
      <Button title="Test Crash" onPress={() => crashlytics().crash()} />
    </View>
  );
}
