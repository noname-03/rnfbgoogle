import {
    View,
    Text,
    Image,
    Dimensions,
    TextInput,
    Button,
    TouchableOpacity,
} from 'react-native';
import React, { useState, useEffect } from 'react';
import TouchID from 'react-native-touch-id';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';

const LoginScreen = ({ navigation }) => {

    useEffect(() => {
        GoogleSignin.configure({
            webClientId: '900843675507-bg3772lp0b4m6ghq8m97b9krmqnvm4s3.apps.googleusercontent.com',
        });
    })

    async function onGoogleButtonPress() {
        // Get the users ID token
        const { idToken } = await GoogleSignin.signIn();

        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(idToken);

        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
    }

    const optionalConfigObject = {
        title: 'Authentication Required', // Android
        imageColor: '#e00606', // Android
        imageErrorColor: '#ff0000', // Android
        sensorDescription: 'Touch sensor', // Android
        sensorErrorDescription: 'Failed', // Android
    };

    const pressHandler = () => {
        TouchID.authenticate('Touch to login', optionalConfigObject)
            .then(success => {
                navigation.navigate('MainApp');
            })
            .catch(error => {
                alert('Authentication Failed');
            });
    }

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // const dispatch = useDispatch()
    const submit = () => {
        // dispatch(Login(email, password))
    };

    return (
        <View style={{ flex: 1, backgroundColor: 'black' }}>
            {/* <Image source={LoginLogo} style={{ margin: 12, width: 400, height: 200 }} /> */}
            <TextInput
                style={{ margin: 12, padding: 10, borderWidth: 1 }}
                placeholder="Email"
                value={email}
                onChangeText={(text) => setEmail(text)}
            />
            <TextInput
                style={{ margin: 12, padding: 10, borderWidth: 1 }}
                placeholder="Password"
                secureTextEntry={true}
                value={password}
                onChangeText={(text) => setPassword(text)}
            />
            <View style={{ margin: 12 }}>
                <Button title="Login" />
            </View>
            <View style={{ margin: 12 }}>
                <Button title="Touch id" onPress={() => pressHandler()} />
            </View>
            <View style={{ margin: 12 }}><Button
                title="Google Sign-In"
                onPress={() => onGoogleButtonPress().then(() => navigation.navigate('MainApp'))}
            />
            </View>
            <View style={{ alignItems: 'center' }}>
                <Text style={{ alignItems: 'center' }}>Don't have an account ?</Text>
                <TouchableOpacity
                >
                    <Text style={{ color: 'blue', fontWeight: 'bold', fontSize: 16 }}>
                        Register
                    </Text>
                </TouchableOpacity>
            </View>
        </View >
    );
};

export default LoginScreen;
